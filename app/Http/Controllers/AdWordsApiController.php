<?php
namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use Google\ApiCore\ApiException;
use Google\Auth\FetchAuthTokenInterface;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\Ads\GoogleAds\Lib\OAuth2TokenBuilder;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsClient;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\Ads\GoogleAds\V2\Errors\GoogleAdsError;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsException;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsClientBuilder;
use Google\AdsApi\AdWords\Query\v201809\ReportQueryBuilder;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Query\v201809\ServiceQueryBuilder;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;

use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;

class AdWordsApiController extends Controller
{
    const PAGE_LIMIT = 500;
    const CUSTOMER_ID = 7455002068;
    
    private static $REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS = [
        'CAMPAIGN_PERFORMANCE_REPORT' => [
            'CampaignId',
            'CampaignName',
            'CampaignStatus',
            'AccountDescriptiveName',
            'Amount',
            'AverageCost',
            'Impressions',
            'Clicks',
            'Ctr',
            'Cost',
            'Conversions',
            'CrossDeviceConversions'
        ],
        'ADGROUP_PERFORMANCE_REPORT' => [
            'AdGroupId',
            'AdGroupName',
            'AdGroupStatus',
            'CampaignId'
        ],
        'AD_PERFORMANCE_REPORT' => ['AdGroupId', 'AdGroupName', 'Id', 'AdType'],
        'ACCOUNT_PERFORMANCE_REPORT' => [
            'AccountDescriptiveName',
            'ExternalCustomerId'
        ],
    ];

    /**
     * Controls a POST and GET request that is submitted from the "Get All
     * Campaigns" form.
     *
     * @param Request $request
     * @param FetchAuthTokenInterface $oAuth2Credential
     * @param AdWordsServices $adWordsServices
     * @param AdWordsSessionBuilder $adWordsSessionBuilder
     * @return View
     */
    public function getCampaignsAction(
        Request $request,
        FetchAuthTokenInterface $oAuth2Credential,
        AdWordsServices $adWordsServices,
        AdWordsSessionBuilder $adWordsSessionBuilder
    ) {
        if ($request->method() === 'POST') {
            // "name" => "Name"
            // "status" => "Status"
            // "servingStatus" => "ServingStatus"
            // "advertisingChannelType" => "AdvertisingChannelType"
            // "advertisingChannelSubType" => "AdvertisingChannelSubType"

            // "impressions" => "Impressions"
            // "clicks" => "Clicks"
            // "ctr" => "Ctr"
            // "cost" => "Cost"
            // "conversions" => "Conversions"
            // "crossDeviceConversions" => "CrossDeviceConversions"
            
            // "reportRange" => "YESTERDAY"
            // "reportType" => "CAMPAIGN_PERFORMANCE_REPORT"
            
            // dd($request->input());
            // Always select at least the "Id" field.
            $selectedFields = array_values(
                ['id' => 'Id'] + $request->except(
                    ['_token', 'clientCustomerId', 'entriesPerPage']
                )
            );
            $clientCustomerId = $request->input('clientCustomerId');
            $entriesPerPage = $request->input('entriesPerPage');

            // Construct an API session configured from a properties file and
            // the OAuth2 credentials above.
            $session =
                $adWordsSessionBuilder->fromFile(config('app.adsapi_php_path'))
                    ->withOAuth2Credential($oAuth2Credential)
                    ->withClientCustomerId($clientCustomerId)
                    ->build();

            // $selectedFields[] = "Impressions";
            // $selectedFields[] = "Clicks";
            // $selectedFields[] = "Ctr";
            // $selectedFields[] = "Cost";
            // $selectedFields[] = "Conversions";
            // $selectedFields[] = "CrossDeviceConversions";
            // dd($selectedFields);
            $request->session()->put('selectedFields', $selectedFields);
            $request->session()->put('entriesPerPage', $entriesPerPage);
            $request->session()->put('session', $session);
        } else {
            $selectedFields = $request->session()->get('selectedFields');
            $entriesPerPage = $request->session()->get('entriesPerPage');
            $session = $request->session()->get('session');
        }

        $pageNo = $request->input('page') ?: 1;
        $collection = self::fetchCampaigns(
            $request,
            $adWordsServices->get($session, CampaignService::class),
            $selectedFields,
            $entriesPerPage,
            $pageNo
        );
        // Create a length aware paginator to supply campaigns for the view,
        // based on the specified number of entries per page.
        $campaigns = new LengthAwarePaginator(
            $collection,
            $request->session()->get('totalNumEntries'),
            $entriesPerPage,
            $pageNo,
            ['path' => url('get-campaigns')]
        );

        return view('campaigns', compact('campaigns', 'selectedFields'));
    }

    /**
     * Fetch campaigns using the provided campaign service, selected fields, the
     * number of entries per page and the specified page number.
     *
     * @param Request $request
     * @param CampaignService $campaignService
     * @param string[] $selectedFields
     * @param int $entriesPerPage
     * @param int $pageNo
     * @return Collection
     */
    private function fetchCampaigns(
        Request $request,
        CampaignService $campaignService,
        array $selectedFields,
        $entriesPerPage,
        $pageNo
    ) {
        $query = (new ServiceQueryBuilder())
            ->select($selectedFields)
            ->orderByAsc('Name')
            ->limit(
                ($pageNo - 1) * $entriesPerPage,
                intval($entriesPerPage)
            )->build();

        $totalNumEntries = 0;
        $results = [];

        $page = $campaignService->query("$query");
        if (!empty($page->getEntries())) {
            $totalNumEntries = $page->getTotalNumEntries();
            $results = $page->getEntries();
        }

        $request->session()->put('totalNumEntries', $totalNumEntries);

        return collect($results);
    }

    /**
     * Controls a POST and GET request that is submitted from the "Download
     * Report" form.
     *
     * @param Request $request
     * @param FetchAuthTokenInterface $oAuth2Credential
     * @param AdWordsServices $adWordsServices
     * @param AdWordsSessionBuilder $adWordsSessionBuilder
     * @return View
     */
    public function downloadReportAction(
        Request $request,
        FetchAuthTokenInterface $oAuth2Credential,
        AdWordsServices $adWordsServices,
        AdWordsSessionBuilder $adWordsSessionBuilder
    ) {
        if ($request->method() === 'POST') {
            // "reportType" => "CAMPAIGN_PERFORMANCE_REPORT"
            // "impressions" => "Impressions"
            // "clicks" => "Clicks"
            // "ctr" => "Ctr"
            // "cost" => "Cost"
            // "conversions" => "Conversions"
            // "crossDeviceConversions" => "CrossDeviceConversions"
            // "reportRange" => "YESTERDAY"
            // "entriesPerPage" => "20"

            // dd($request->input());
            $clientCustomerId = $request->input('clientCustomerId');
            $reportType = 'CAMPAIGN_PERFORMANCE_REPORT';
            $reportRange = $request->input('reportRange');
            $entriesPerPage = 500;

            $selectedFields = array_values(
                $request->except(
                    [
                        '_token',
                        'clientCustomerId',
                        'reportType',
                        'entriesPerPage',
                        'reportRange'
                    ]
                )
            );
            $selectedFields = array_merge(
                self::$REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS[$reportType],
                $selectedFields
            );

            $request->session()->put('clientCustomerId', $clientCustomerId);
            $request->session()->put('reportType', $reportType);
            $request->session()->put('reportRange', $reportRange);
            $request->session()->put('selectedFields', $selectedFields);
            $request->session()->put('entriesPerPage', $entriesPerPage);

            // Construct an API session configured from a properties file and
            // the OAuth2 credentials above.
            $session =
                $adWordsSessionBuilder->fromFile(config('app.adsapi_php_path'))
                    ->withOAuth2Credential($oAuth2Credential)
                    ->withClientCustomerId($clientCustomerId)
                    ->build();

            // There is no paging mechanism for reporting, so we fetch all
            // results at once.
            $collection = self::downloadReport(
                $reportType,
                $reportRange,
                new ReportDownloader($session),
                $selectedFields
            );
            $request->session()->put('collection', $collection);
        } else {
            $selectedFields = $request->session()->get('selectedFields');
            $entriesPerPage = $request->session()->get('entriesPerPage');
            $collection = $request->session()->get('collection');
        }

        $pageNo = $request->input('page') ?: 1;

        // Create a length aware paginator to supply report results for the
        // view, based on the specified number of entries per page.
        $reportResults = new LengthAwarePaginator(
            $collection->forPage($pageNo, $entriesPerPage),
            $collection->count(),
            $entriesPerPage,
            $pageNo,
            ['path' => url('download-report')]
        );

        return view(
            'report-results',
            compact('reportResults', 'selectedFields')
        );
    }

    /**
     * Download a report of the specified report type and date range, selected
     * fields, and the number of entries per page.
     *
     * @param string $reportType
     * @param string $reportRange
     * @param ReportDownloader $reportDownloader
     * @param string[] $selectedFields
     * @return Collection
     */
    private function downloadReport(
        $reportType,
        $reportRange,
        ReportDownloader $reportDownloader,
        array $selectedFields
    ) {
        $query = (new ReportQueryBuilder())
            ->select($selectedFields)
            ->from($reportType)
            ->duringDateRange($reportRange)->build();

        // For brevity, this sample app always excludes zero-impression rows.
        $reportSettingsOverride = (new ReportSettingsBuilder())
            ->includeZeroImpressions(true)
            ->build();
        $reportDownloadResult = $reportDownloader->downloadReportWithAwql(
            "$query",
            DownloadFormat::XML,
            $reportSettingsOverride
        );
        
        $json = json_encode(
            simplexml_load_string($reportDownloadResult->getAsString())
        );
        
        $resultTable = json_decode($json, true)['table'];
        if (array_key_exists('row', $resultTable)) {
            $row = $resultTable['row'];
            // dd($row);
        }

        if (array_key_exists('row', $resultTable)) {
            // When there is only one row, PHP decodes it by automatically
            // removing the containing array. We need to add it back, so the
            // "view" can render this data properly.
            $row = $resultTable['row'];
            $row = count($row) > 1 ? $row : [$row];
            return collect($row);
        }

        // No results returned for this query.
        return collect([]);
    }

    public static function customers()
    {
        // $oAuth2Credential = (new OAuth2TokenBuilder())
        //     ->withClientId('INSERT_CLIENT_ID')
        //     ->withClientSecret('INSERT_CLIENT_SECRET')
        //     ->withRefreshToken('INSERT_REFRESH_TOKEN')
        // ->build();
        // $googleAdsClient = (new GoogleAdsClientBuilder())
        //     ->withOAuth2Credential($oAuth2Credential)
        //     ->withDeveloperToken('INSERT_DEVELOPER_TOKEN_HERE')
        //     ->withLoginCustomerId('INSERT_LOGIN_CUSTOMER_ID_HERE')
        // ->build();
        //dd(parse_ini_file(base_path('google_ads_php.ini')));
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile(base_path('google_ads_php.ini'))->build();

        // Construct a Google Ads client configured from a properties file and the
        // OAuth2 credentials above.
        $googleAdsClient = (new GoogleAdsClientBuilder())->fromFile(base_path('google_ads_php.ini'))
            ->withOAuth2Credential($oAuth2Credential)
            ->build();
        try {
            self::runGetCustomers($googleAdsClient);
        } catch (GoogleAdsException $googleAdsException) {
            printf(
                "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
                $googleAdsException->getRequestId(),
                PHP_EOL,
                PHP_EOL
            );
            foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
                /** @var GoogleAdsError $error */
                printf(
                    "\t%s: %s%s",
                    $error->getErrorCode()->getErrorCode(),
                    $error->getMessage(),
                    PHP_EOL
                );
            }
        } catch (ApiException $apiException) {
            printf(
                "ApiException was thrown with message '%s'.%s",
                $apiException->getMessage(),
                PHP_EOL
            );
        }
    }

    /**
     * Runs the example.
     *
     * @param GoogleAdsClient $googleAdsClient the Google Ads API client
     */
    public static function runGetCustomers(GoogleAdsClient $googleAdsClient)
    {
        $customerServiceClient = $googleAdsClient->getCustomerServiceClient();

        // Issues a request for listing all accessible customers.
        $accessibleCustomers = $customerServiceClient->listAccessibleCustomers();
        
        print 'Total results: ' . count($accessibleCustomers->getResourceNames()) . PHP_EOL;

        // Iterates over all accessible customers' resource names and prints them.
        foreach ($accessibleCustomers->getResourceNames() as $resourceName) {
            /** @var string $resourceName */
            printf("Customer resource name: '%s'%s", $resourceName, PHP_EOL);
        }
    }

    public static function getAccountHierarchy() {
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId('641584981751-77o0iibobfq4c5vpi3mdbvlgbij703gb.apps.googleusercontent.com')
            ->withClientSecret('jcClZlKYILMNG48nkwJdX1c-')
            ->withRefreshToken('1//0e4JJzkK0HOaiCgYIARAAGA4SNwF-L9IrnffxoVummchM-ZU7rNHv6XswflZBkKHPC80iJ6oL6EKvboUpTqq2Ab-vR11E00DfTms')
            ->build();
        
        $session = (new AdWordsSessionBuilder())
            ->withDeveloperToken('xBXyzGg-RkJwazCaGS2GCQ')
            ->withClientCustomerId('7455002068')
            ->withOAuth2Credential($oAuth2Credential
            )->build();

        self::runGetAccount(new AdWordsServices(), $session);
    }

    public static function runGetAccount(
        AdWordsServices $adWordsServices,
        AdWordsSession $session
    ) {
        $managedCustomerService = $adWordsServices->get(
            $session,
            ManagedCustomerService::class
        );

        // Create selector.
        $selector = new Selector();
        $selector->setFields(['CustomerId', 'Name']);
        $selector->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));

        // Maps from customer IDs to accounts and links.
        $customerIdsToAccounts = [];
        $customerIdsToChildLinks = [];
        $customerIdsToParentLinks = [];

        $totalNumEntries = 0;
        do {
            // Make the get request.
            $page = $managedCustomerService->get($selector);

            // Create links between manager and clients.
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                if ($page->getLinks() !== null) {
                    foreach ($page->getLinks() as $link) {
                        // Cast the indexes to string to avoid the issue when 32-bit PHP
                        // automatically changes the IDs that are larger than the 32-bit max
                        // integer value to negative numbers.
                        $managerCustomerId = strval($link->getManagerCustomerId());
                        $customerIdsToChildLinks[$managerCustomerId][] = $link;
                        $clientCustomerId = strval($link->getClientCustomerId());
                        $customerIdsToParentLinks[$clientCustomerId] = $link;
                    }
                }
                foreach ($page->getEntries() as $account) {
                    $customerIdsToAccounts[strval($account->getCustomerId())] = $account;
                }
            }

            // Advance the paging index.
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);

        // Find the root account.
        $rootAccount = null;
        foreach ($customerIdsToAccounts as $account) {
            if (!array_key_exists(
                $account->getCustomerId(),
                $customerIdsToParentLinks
            )) {
                $rootAccount = $account;
                break;
            }
        }

        if ($rootAccount !== null) {
            // Display results.
            self::printAccountHierarchy(
                $rootAccount,
                $customerIdsToAccounts,
                $customerIdsToChildLinks
            );
        } else {
            printf("No accounts were found.\n");
        } 
    }

    private static function printAccountHierarchy(
        $account,
        $customerIdsToAccounts,
        $customerIdsToChildLinks,
        $depth = null
    ) {
        if ($depth === null) {
            print "(Customer ID, Account Name)\n";
            self::printAccountHierarchy(
                $account,
                $customerIdsToAccounts,
                $customerIdsToChildLinks,
                0
            );

            return;
        }

        print str_repeat('-', $depth * 2);
        $customerId = $account->getCustomerId();
        printf("%s, %s\n", $customerId, $account->getName());

        if (array_key_exists($customerId, $customerIdsToChildLinks)) {
            foreach ($customerIdsToChildLinks[strval($customerId)] as $childLink) {
                $childAccount = $customerIdsToAccounts[strval($childLink->getClientCustomerId())];
                self::printAccountHierarchy(
                    $childAccount,
                    $customerIdsToAccounts,
                    $customerIdsToChildLinks,
                    $depth + 1
                );
            }
        }
    }
}

