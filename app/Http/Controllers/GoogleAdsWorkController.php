<?php
namespace App\Http\Controllers;

use GetOpt\GetOpt;
use Google\AdsApi\AdWords\v201809\cm\AdvertisingChannelType;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyType;
use Google\AdsApi\AdWords\v201809\cm\Budget;
use Google\AdsApi\AdWords\v201809\cm\BudgetBudgetDeliveryMethod;
use Google\AdsApi\AdWords\v201809\cm\BudgetOperation;
use Google\AdsApi\AdWords\v201809\cm\BudgetService;
use Google\AdsApi\AdWords\v201809\cm\Campaign;
use Google\AdsApi\AdWords\v201809\cm\CampaignOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignStatus;
use Google\AdsApi\AdWords\v201809\cm\FrequencyCap;
use Google\AdsApi\AdWords\v201809\cm\GeoTargetTypeSetting;
use Google\AdsApi\AdWords\v201809\cm\GeoTargetTypeSettingNegativeGeoTargetType;
use Google\AdsApi\AdWords\v201809\cm\GeoTargetTypeSettingPositiveGeoTargetType;
use Google\AdsApi\AdWords\v201809\cm\Level;
use Google\AdsApi\AdWords\v201809\cm\ManualCpcBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\NetworkSetting;
use Google\AdsApi\AdWords\v201809\cm\TimeUnit;
use Illuminate\View\View;
use Illuminate\Http\Request;

use App\Helpers\ArgumentNames;
use Google\Protobuf\BoolValue;
use App\Helpers\ArgumentParser;
use Google\ApiCore\ApiException;

use Google\Protobuf\StringValue;
use Illuminate\Support\Collection;
use Google\Auth\FetchAuthTokenInterface;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\Ads\GoogleAds\Util\V2\ResourceNames;
use Google\Ads\GoogleAds\V2\Resources\Customer;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Illuminate\Pagination\LengthAwarePaginator;
use Google\Ads\GoogleAds\Lib\OAuth2TokenBuilder;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsClient;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;

use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\Ads\GoogleAds\V2\Errors\GoogleAdsError;
use Google\Ads\GoogleAds\V2\Services\GoogleAdsRow;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsException;
use Google\AdsApi\AdWords\v201809\cm\AdGroupService;

use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsClientBuilder;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\Ads\GoogleAds\V2\Resources\CampaignExperiment;
use Google\AdsApi\AdWords\Query\v201809\ReportQueryBuilder;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Query\v201809\ServiceQueryBuilder;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;
use Google\Ads\GoogleAds\V2\Enums\CampaignExperimentTrafficSplitTypeEnum\CampaignExperimentTrafficSplitType;

use Google\AdsApi\AdWords\v201809\cm\AdGroup;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdRotationMode;
use Google\AdsApi\AdWords\v201809\cm\AdGroupOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupStatus;
use Google\AdsApi\AdWords\v201809\cm\AdRotationMode;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyConfiguration;
use Google\AdsApi\AdWords\v201809\cm\CpcBid;
use Google\AdsApi\AdWords\v201809\cm\CriterionTypeGroup;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\TargetingSetting;
use Google\AdsApi\AdWords\v201809\cm\TargetingSettingDetail;
// use Google\AdsApi\Common\OAuth2TokenBuilder;

class GoogleAdsWorkController extends Controller
{
    const PAGE_LIMIT = 500;

    public static $REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS = [
        'CAMPAIGN_PERFORMANCE_REPORT' => [
            'CampaignId',
            'CampaignName',
            'CampaignStatus',
            'AccountDescriptiveName',
            'Amount',
            'AverageCost',
            'Impressions',
            'Clicks',
            'Ctr',
            'Cost',
            'Conversions',
            'CrossDeviceConversions'
        ],
        'ADGROUP_PERFORMANCE_REPORT' => [
            'AdGroupId',
            'AdGroupName',
            'AdGroupStatus',
            'CampaignId'
        ],
        'AD_PERFORMANCE_REPORT' => ['AdGroupId', 'AdGroupName', 'Id', 'AdType'],
        'ACCOUNT_PERFORMANCE_REPORT' => [
            'AccountDescriptiveName',
            'ExternalCustomerId'
        ],
    ];

    public function getCustomersAndFormatData() {
        $customers = $this->getCustomers();
        
        $totalAccountOfAllCustomerID = [];
        if(sizeof($customers > 0)) {
            foreach($customers as $customer) {
                foreach($customer as $item) {
                    $totalAccountOfAllCustomerID[] = $item;
                }
            }
            return $totalAccountOfAllCustomerID;
        } else {
            foreach($customers[0] as $customer) {
                $totalAccountOfAllCustomerID[] = $customer;
            }
            return $totalAccountOfAllCustomerID;
        }
    }

    public function getAllCampaignOfAllAccounts() {

        $campaigns = $this->getCampaigns();
        // Loop the data to render to format
        // $array = [
        //     "data" => [
        //         "id"    => 'campaignId',
        //         "accountType"  =>  MarketingVariables::GOOGLE_TYPE,
        //         "name"  => 'campaignName',
        //         "status"    => 'ACTIVE',
        //         "accountName"    => 'accountName',
        //         "type"    => 'type',
        //         "impressions"    => 'impressions',
        //         "clicks"    => 'clicks',
        //         "totalContacts"    => 'totalContacts',
        //         "customers"    => 'customers',
        //         "costPerContact"    => 'costPerContact',
        //         "amountSpent"    => 'amountSpent',
        //         "revenue"    => 'revenue',
        //         "roi"    => 'roi',
        //         "leads"    => 'leads'
        //     ],
        //     "sum_impressions"  => 0,
        //     "sum_clicks"  => 0,
        //     "sum_contact"  => 0,
        // ];
    }

    public function getCampaigns(
        Request $request,
        FetchAuthTokenInterface $oAuth2Credential,
        AdWordsServices $adWordsServices,
        AdWordsSessionBuilder $adWordsSessionBuilder
    ) {
        // if ($request->method() === 'POST') {
            // "reportType" => "CAMPAIGN_PERFORMANCE_REPORT"
            // "impressions" => "Impressions"
            // "clicks" => "Clicks"
            // "ctr" => "Ctr"
            // "cost" => "Cost"
            // "conversions" => "Conversions"
            // "crossDeviceConversions" => "CrossDeviceConversions"
            // "reportRange" => "YESTERDAY"
            // "entriesPerPage" => "20"

            // dd($request->input());
            $clientCustomerId = $request->input('clientCustomerId');
            $reportType = 'CAMPAIGN_PERFORMANCE_REPORT';
            $reportRange = $request->input('reportRange');
            $entriesPerPage = 500;

            $selectedFields = array_values(
                $request->except(
                    [
                        '_token',
                        'clientCustomerId',
                        'reportType',
                        'entriesPerPage',
                        'reportRange'
                    ]
                )
            );
            $selectedFields = array_merge(
                self::$REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS[$reportType],
                $selectedFields
            );

            $request->session()->put('clientCustomerId', $clientCustomerId);
            $request->session()->put('reportType', $reportType);
            $request->session()->put('reportRange', $reportRange);
            $request->session()->put('selectedFields', $selectedFields);
            $request->session()->put('entriesPerPage', $entriesPerPage);

            // Construct an API session configured from a properties file and
            // the OAuth2 credentials above.
            $session =
                $adWordsSessionBuilder->fromFile(config('app.adsapi_php_path'))
                    ->withOAuth2Credential($oAuth2Credential)
                    ->withClientCustomerId($clientCustomerId)
                    ->build();

            // There is no paging mechanism for reporting, so we fetch all
            // results at once.
            $collection = self::downloadReport(
                $reportType,
                $reportRange,
                new ReportDownloader($session),
                $selectedFields
            );
            return $collection;
            // $request->session()->put('collection', $collection);
        // } else {
        //     $selectedFields = $request->session()->get('selectedFields');
        //     $entriesPerPage = $request->session()->get('entriesPerPage');
        //     $collection = $request->session()->get('collection');
        // }

        // $pageNo = $request->input('page') ?: 1;

        // Create a length aware paginator to supply report results for the
        // view, based on the specified number of entries per page.
        // $reportResults = new LengthAwarePaginator(
        //     $collection->forPage($pageNo, $entriesPerPage),
        //     $collection->count(),
        //     $entriesPerPage,
        //     $pageNo,
        //     ['path' => url('download-report')]
        // );

        // return view(
        //     'report-results',
        //     compact('reportResults', 'selectedFields')
        // );
    }

    public function downloadReport(
        $reportType,
        $reportRange,
        ReportDownloader $reportDownloader,
        array $selectedFields
    ) {
        $query = (new ReportQueryBuilder())
            ->select($selectedFields)
            ->from($reportType)
            ->duringDateRange($reportRange)->build();

        // For brevity, this sample app always excludes zero-impression rows.
        $reportSettingsOverride = (new ReportSettingsBuilder())
            ->includeZeroImpressions(true)
            ->build();
        $reportDownloadResult = $reportDownloader->downloadReportWithAwql(
            "$query",
            DownloadFormat::XML,
            $reportSettingsOverride
        );
        
        $json = json_encode(
            simplexml_load_string($reportDownloadResult->getAsString())
        );
        
        $resultTable = json_decode($json, true)['table'];
        if (array_key_exists('row', $resultTable)) {
            $row = $resultTable['row'];
            return $row;
        }

        // if (array_key_exists('row', $resultTable)) {
        //     // When there is only one row, PHP decodes it by automatically
        //     // removing the containing array. We need to add it back, so the
        //     // "view" can render this data properly.
        //     $row = $resultTable['row'];
        //     $row = count($row) > 1 ? $row : [$row];
        //     return collect($row);
        // }

        // // No results returned for this query.
        return [];
    }

    public function getCustomers()
    {
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId('641584981751-77o0iibobfq4c5vpi3mdbvlgbij703gb.apps.googleusercontent.com')
            ->withClientSecret('jcClZlKYILMNG48nkwJdX1c-')
            ->withRefreshToken('1//0e4JJzkK0HOaiCgYIARAAGA4SNwF-L9IrnffxoVummchM-ZU7rNHv6XswflZBkKHPC80iJ6oL6EKvboUpTqq2Ab-vR11E00DfTms')
            ->build();

        $googleAdsClient = (new GoogleAdsClientBuilder())
            ->withOAuth2Credential($oAuth2Credential)
            ->withDeveloperToken('xBXyzGg-RkJwazCaGS2GCQ')
        ->build();

        try {
            $customerServiceClient = $googleAdsClient->getCustomerServiceClient();
            
            // Issues a request for listing all accessible customers.
            $accessibleCustomers = $customerServiceClient->listAccessibleCustomers();
            // print 'Total results: ' . count($accessibleCustomers->getResourceNames()) . PHP_EOL;
            // Iterates over all accessible customers' resource names and prints them.
            $result = [];
            foreach ($accessibleCustomers->getResourceNames() as $resourceName) {
                $customerID = str_replace('customers/','',$resourceName);
                /** @var string $resourceName */
                // printf("Customer resource name: '%s'%s", $resourceName, PHP_EOL);
                // Get account information
                if(!self::getCustomersDetail($customerID) == []) {
                    $result[] = self::getCustomersDetail($customerID);
                }
            }
            return $result;

        } catch (GoogleAdsException $googleAdsException) {
            // printf(
            //     "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
            //     $googleAdsException->getRequestId(),
            //     PHP_EOL,
            //     PHP_EOL
            // );
            // foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
            //     printf(
            //         "\t%s: %s%s",
            //         $error->getErrorCode()->getErrorCode(),
            //         $error->getMessage(),
            //         PHP_EOL
            //     );
            // }
        } catch (ApiException $apiException) {
            // printf(
            //     "ApiException was thrown with message '%s'.%s",
            //     $apiException->getMessage(),
            //     PHP_EOL
            // );
        }
    }

    public function getCustomersDetail($customerId)
    {
        // Either pass the required parameters for this example on the command line, or insert them
        // into the constants above.
        $options = (new ArgumentParser())->parseCommandArguments([
            ArgumentNames::CUSTOMER_ID => GetOpt::REQUIRED_ARGUMENT
        ]);
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId('641584981751-77o0iibobfq4c5vpi3mdbvlgbij703gb.apps.googleusercontent.com')
            ->withClientSecret('jcClZlKYILMNG48nkwJdX1c-')
            ->withRefreshToken('1//0e4JJzkK0HOaiCgYIARAAGA4SNwF-L9IrnffxoVummchM-ZU7rNHv6XswflZBkKHPC80iJ6oL6EKvboUpTqq2Ab-vR11E00DfTms')
            ->build();
        // Construct a Google Ads client configured from a properties file and the
        // OAuth2 credentials above.
        $googleAdsClient = (new GoogleAdsClientBuilder())
            ->withOAuth2Credential($oAuth2Credential)
            ->withDeveloperToken('xBXyzGg-RkJwazCaGS2GCQ')
        ->build();

        try {
            $customerId = $options[ArgumentNames::CUSTOMER_ID] ?: $customerId;

            // Issues a getCustomer() request and gets the result.
            $customerServiceClient = $googleAdsClient->getCustomerServiceClient();
            $customer = $customerServiceClient->getCustomer(ResourceNames::forCustomer($customerId));
            // Print information about the account.
            // printf(
            //     "Customer with ID %d, descriptive name '%s', currency code '%s', timezone '%s', "
            //     . "tracking URL template '%s' and auto tagging enabled '%s' was retrieved.%s",
            //     $customer->getId()->getValue(),
            //     $customer->getDescriptiveName()->getValue(),
            //     $customer->getCurrencyCode()->getValue(),
            //     $customer->getTimeZone()->getValue(),
            //     is_null($customer->getTrackingUrlTemplate())
            //         ? 'N/A' : $customer->getTrackingUrlTemplate()->getValue(),
            //     $customer->getAutoTaggingEnabled()->getValue() ? 'true' : 'false',
            //     PHP_EOL
            // );
            
            // return [
            //     "type"  =>  2,
            //     "id"    => $customer->getId()->getValue(),
            //     "name"  => $customer->getDescriptiveName()->getValue(),
            //     "account_id"    => $customer->getId()->getValue()
            // ];
            return self::getAccountHierarchy($customer->getId()->getValue());

        } catch (GoogleAdsException $googleAdsException) {
            // printf(
            //     "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
            //     $googleAdsException->getRequestId(),
            //     PHP_EOL,
            //     PHP_EOL
            // );
            // foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
            //     /** @var GoogleAdsError $error */
            //     printf(
            //         "\t%s: %s%s",
            //         $error->getErrorCode()->getErrorCode(),
            //         $error->getMessage(),
            //         PHP_EOL
            //     );
            // }
        } catch (ApiException $apiException) {
            // printf(
            //     "ApiException was thrown with message '%s'.%s",
            //     $apiException->getMessage(),
            //     PHP_EOL
            // );
        }
    }

    public static function getAccountHierarchy($customerId) {
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId('641584981751-77o0iibobfq4c5vpi3mdbvlgbij703gb.apps.googleusercontent.com')
            ->withClientSecret('jcClZlKYILMNG48nkwJdX1c-')
            ->withRefreshToken('1//0e4JJzkK0HOaiCgYIARAAGA4SNwF-L9IrnffxoVummchM-ZU7rNHv6XswflZBkKHPC80iJ6oL6EKvboUpTqq2Ab-vR11E00DfTms')
            ->build();
        
        $session = (new AdWordsSessionBuilder())
            ->withDeveloperToken('xBXyzGg-RkJwazCaGS2GCQ')
            ->withClientCustomerId($customerId)
            ->withOAuth2Credential($oAuth2Credential
            )->build();

        return self::runGetAccount(new AdWordsServices(), $session);
    }

    public static function runGetAccount(
        AdWordsServices $adWordsServices,
        AdWordsSession $session
    ) {
        $managedCustomerService = $adWordsServices->get(
            $session,
            ManagedCustomerService::class
        );

        // Create selector.
        $selector = new Selector();
        $selector->setFields(['CustomerId', 'Name']);
        $selector->setOrdering([new OrderBy('CustomerId', SortOrder::ASCENDING)]);
        $selector->setPaging(new Paging(0, self::PAGE_LIMIT));

        // Maps from customer IDs to accounts and links.
        $customerIdsToAccounts = [];
        $customerIdsToChildLinks = [];
        $customerIdsToParentLinks = [];

        $totalNumEntries = 0;
        do {
            // Make the get request.
            $page = $managedCustomerService->get($selector);

            // Create links between manager and clients.
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                if ($page->getLinks() !== null) {
                    foreach ($page->getLinks() as $link) {
                        // Cast the indexes to string to avoid the issue when 32-bit PHP
                        // automatically changes the IDs that are larger than the 32-bit max
                        // integer value to negative numbers.
                        $managerCustomerId = strval($link->getManagerCustomerId());
                        $customerIdsToChildLinks[$managerCustomerId][] = $link;
                        $clientCustomerId = strval($link->getClientCustomerId());
                        $customerIdsToParentLinks[$clientCustomerId] = $link;
                    }
                }
                foreach ($page->getEntries() as $account) {
                    $customerIdsToAccounts[strval($account->getCustomerId())] = $account;
                }
            }

            // Advance the paging index.
            $selector->getPaging()->setStartIndex(
                $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            );
        } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);

        // Find the root account.
        $rootAccount = null;
        foreach ($customerIdsToAccounts as $account) {
            if (!array_key_exists(
                $account->getCustomerId(),
                $customerIdsToParentLinks
            )) {
                $rootAccount = $account;
                break;
            }
        }

        if ($rootAccount !== null) {
            return self::printAccountHierarchy(
                $rootAccount,
                $customerIdsToAccounts,
                $customerIdsToChildLinks
            );
        } else {
            printf("No accounts were found.\n");
        } 
    }

    private static function printAccountHierarchy(
        $account,
        $customerIdsToAccounts,
        $customerIdsToChildLinks,
        $depth = null
    ) {
        // dd($account);
        // ManagedCustomer {#5045 ▼
        //     #name: "Dev Ads Test"
        //     #customerId: 7455002068
        //     #canManageClients: null
        //     #currencyCode: null
        //     #dateTimeZone: null
        //     #testAccount: null
        //     #accountLabels: null
        //     #excludeHiddenAccounts: null
        //   }
        // dd($customerIdsToAccounts);  // => 6 items
        // dd($customerIdsToChildLinks);   // => 5 items
        
        $customerId = $account->getCustomerId();
        $result = [];
        if (array_key_exists($customerId, $customerIdsToChildLinks)) {
            foreach ($customerIdsToChildLinks[strval($customerId)] as $childLink) {
                $childAccount = $customerIdsToAccounts[strval($childLink->getClientCustomerId())];
                $result[] = [
                    "type"  =>  2,
                    "id"    => $childAccount->getCustomerId(),
                    "name"  => $childAccount->getName(),
                    "account_id"    => $childAccount->getCustomerId()
                ];
            }
        }
        return $result;


        // if ($depth === null) {
        //     print "(Customer ID, Account Name)\n";
        //     self::printAccountHierarchy(
        //         $account,
        //         $customerIdsToAccounts,
        //         $customerIdsToChildLinks,
        //         0
        //     );

        //     return;
        // }

        // print str_repeat('-', $depth * 2);
        // $customerId = $account->getCustomerId();
        // printf("%s, %s\n", $customerId, $account->getName());

        // if (array_key_exists($customerId, $customerIdsToChildLinks)) {
        //     foreach ($customerIdsToChildLinks[strval($customerId)] as $childLink) {
        //         $childAccount = $customerIdsToAccounts[strval($childLink->getClientCustomerId())];
        //         self::printAccountHierarchy(
        //             $childAccount,
        //             $customerIdsToAccounts,
        //             $customerIdsToChildLinks,
        //             $depth + 1
        //         );
        //     }
        // }
    }

    // Old code
    public function getAdGroups1($customerID, $campaignID) {
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId('641584981751-77o0iibobfq4c5vpi3mdbvlgbij703gb.apps.googleusercontent.com')
            ->withClientSecret('jcClZlKYILMNG48nkwJdX1c-')
            ->withRefreshToken('1//0e4JJzkK0HOaiCgYIARAAGA4SNwF-L9IrnffxoVummchM-ZU7rNHv6XswflZBkKHPC80iJ6oL6EKvboUpTqq2Ab-vR11E00DfTms')
            ->build();

        $session = (new AdWordsSessionBuilder())
            ->withDeveloperToken('xBXyzGg-RkJwazCaGS2GCQ')
            ->withClientCustomerId($customerID)
            ->withOAuth2Credential($oAuth2Credential
            )->build();

        try {
            self::runGetAdGroup(
                new AdWordsServices(),
                $session,
                intval($campaignID)
            );
        } catch (GoogleAdsException $googleAdsException) {
            printf(
                "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
                $googleAdsException->getRequestId(),
                PHP_EOL,
                PHP_EOL
            );
            foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
                /** @var GoogleAdsError $error */
                printf(
                    "\t%s: %s%s",
                    $error->getErrorCode()->getErrorCode(),
                    $error->getMessage(),
                    PHP_EOL
                );
            }
        } catch (ApiException $apiException) {
            printf(
                "ApiException was thrown with message '%s'.%s",
                $apiException->getMessage(),
                PHP_EOL
            );
        }
    }

    public static function runGetAdGroup(
        AdWordsServices $adWordsServices,
        AdWordsSession $session,
        $campaignId
    ) {
        $adGroupService = $adWordsServices->get($session, AdGroupService::class);
        // Create a selector to select all ad groups for the specified campaign.
        $selector = new Selector();
        $selector->setFields([
            'Id', 'CampaignId', 'CampaignName', 'Name', 'Status', 'Settings',
            'Labels', 'ContentBidCriterionTypeGroup', 'BaseCampaignId', 'BaseAdGroupId',
            'TrackingUrlTemplate', 'FinalUrlSuffix', 'UrlCustomParameters', 'AdGroupType'
            ]);
        $selector->setOrdering([new OrderBy('Name', SortOrder::ASCENDING)]);
        $selector->setPredicates(
            [new Predicate('CampaignId', PredicateOperator::IN, [$campaignId])]
        );
        // $selector->setPaging(new Paging(0, 1000));
        $totalNumEntries = 0;
        // do {
            // Retrieve ad groups one page at a time, continuing to request pages
            // until all ad groups have been retrieved.
            $page = $adGroupService->get($selector);
            // Print out some information for each ad group.
            if ($page->getEntries() !== null) {
                $totalNumEntries = $page->getTotalNumEntries();
                foreach ($page->getEntries() as $adGroup) {
                    print_r($adGroup);
                    printf(
                        "Ad group with ID %d and name '%s' was found.\n",
                        $adGroup->getId(),
                        $adGroup->getName()
                    );
                }
            }
            // $selector->getPaging()->setStartIndex(
            //     $selector->getPaging()->getStartIndex() + self::PAGE_LIMIT
            // );
        // } while ($selector->getPaging()->getStartIndex() < $totalNumEntries);
        printf("Number of results found: %d\n", $totalNumEntries);
    }

    // New code
    public function getAdGroups($customerID, $campaignID) {
        // Either pass the required parameters for this example on the command line, or insert them
        // into the constants above.
        $options = (new ArgumentParser())->parseCommandArguments([
            ArgumentNames::CUSTOMER_ID => GetOpt::REQUIRED_ARGUMENT,
            ArgumentNames::CAMPAIGN_ID => GetOpt::OPTIONAL_ARGUMENT
        ]);
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId('641584981751-77o0iibobfq4c5vpi3mdbvlgbij703gb.apps.googleusercontent.com')
            ->withClientSecret('jcClZlKYILMNG48nkwJdX1c-')
            ->withRefreshToken('1//0e4JJzkK0HOaiCgYIARAAGA4SNwF-L9IrnffxoVummchM-ZU7rNHv6XswflZBkKHPC80iJ6oL6EKvboUpTqq2Ab-vR11E00DfTms')
            ->build();
        // Construct a Google Ads client configured from a properties file and the
        // OAuth2 credentials above.
        $googleAdsClient = (new GoogleAdsClientBuilder())
            ->withOAuth2Credential($oAuth2Credential)
            ->withDeveloperToken('xBXyzGg-RkJwazCaGS2GCQ')
            ->withLoginCustomerId($customerID)
            ->build();
        try {
            self::runExample(
                $googleAdsClient,
                $options[ArgumentNames::CUSTOMER_ID] ?: $customerID,
                $options[ArgumentNames::CAMPAIGN_ID] ?: $campaignID
            );
        } catch (GoogleAdsException $googleAdsException) {
            printf(
                "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
                $googleAdsException->getRequestId(),
                PHP_EOL,
                PHP_EOL
            );
            foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
                /** @var GoogleAdsError $error */
                printf(
                    "\t%s: %s%s",
                    $error->getErrorCode()->getErrorCode(),
                    $error->getMessage(),
                    PHP_EOL
                );
            }
        } catch (ApiException $apiException) {
            printf(
                "ApiException was thrown with message '%s'.%s",
                $apiException->getMessage(),
                PHP_EOL
            );
        }
    }
    /**
     * Runs the example.
     *
     * @param GoogleAdsClient $googleAdsClient the Google Ads API client
     * @param int $customerId the customer ID
     * @param int|null $campaignId the campaign ID for which ad groups will be retrieved. If `null`,
     *     returns from all campaigns
     */
    public static function runExample(
        GoogleAdsClient $googleAdsClient,
        int $customerId,
        ?int $campaignId
    ) {
        $googleAdsServiceClient = $googleAdsClient->getGoogleAdsServiceClient();
        // Creates a query that retrieves all ad groups.
        $query = 'SELECT campaign.id, ad_group.id, ad_group.name FROM ad_group';
        if ($campaignId !== null) {
            $query .= " WHERE campaign.id = $campaignId";
        }
        // Issues a search request by specifying page size.
        $response = $googleAdsServiceClient->search($customerId, $query, ['pageSize' => 1000]);
        dd($response);
        // Iterates over all rows in all pages and prints the requested field values for
        // the ad group in each row.
        foreach ($response->iterateAllElements() as $googleAdsRow) {
            /** @var GoogleAdsRow $googleAdsRow */
            printf(
                "Ad group with ID %d and name '%s' was found in campaign with ID %d.%s",
                $googleAdsRow->getAdGroup()->getId()->getValue(),
                $googleAdsRow->getAdGroup()->getName()->getValue(),
                $googleAdsRow->getCampaign()->getId()->getValue(),
                PHP_EOL
            );
        }
    }

    public function createCampaign($customerID) {
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId('641584981751-77o0iibobfq4c5vpi3mdbvlgbij703gb.apps.googleusercontent.com')
            ->withClientSecret('jcClZlKYILMNG48nkwJdX1c-')
            ->withRefreshToken('1//0e4JJzkK0HOaiCgYIARAAGA4SNwF-L9IrnffxoVummchM-ZU7rNHv6XswflZBkKHPC80iJ6oL6EKvboUpTqq2Ab-vR11E00DfTms')
            ->build();

        // Construct an API session configured from a properties file and the
        // OAuth2 credentials above.
        $session = (new AdWordsSessionBuilder())
            ->withDeveloperToken('xBXyzGg-RkJwazCaGS2GCQ')
            ->withClientCustomerId($customerID)
            ->withOAuth2Credential($oAuth2Credential
            )->build();

        return self::createCampaignExecute(new AdWordsServices(), $session);
    }

    public static function createCampaignExecute(AdWordsServices $adWordsServices, AdWordsSession $session) {
        $budgetService = $adWordsServices->get($session, BudgetService::class);

        // Create the shared budget (required).
        $budget = new Budget();
        $budget->setName('Interplanetary Cruise Budget #' . uniqid());
        $money = new Money();
        $money->setMicroAmount(50000000);
        $budget->setAmount($money);
        $budget->setDeliveryMethod(BudgetBudgetDeliveryMethod::STANDARD);

        $operations = [];

        // Create a budget operation.
        $operation = new BudgetOperation();
        $operation->setOperand($budget);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;

        // Create the budget on the server.
        $result = $budgetService->mutate($operations);
        $budget = $result->getValue()[0];

        $campaignService = $adWordsServices->get($session, CampaignService::class);

        $operations = [];

        // Create a campaign with required and optional settings.
        $campaign = new Campaign();
        $campaign->setName('Interplanetary Cruise #' . uniqid());
        $campaign->setAdvertisingChannelType(AdvertisingChannelType::SEARCH);

        // Set shared budget (required).
        $campaign->setBudget(new Budget());
        $campaign->getBudget()->setBudgetId($budget->getBudgetId());

        // Set bidding strategy (required).
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType(
            BiddingStrategyType::MANUAL_CPC
        );

        // You can optionally provide a bidding scheme in place of the type.
        $biddingScheme = new ManualCpcBiddingScheme();
        $biddingStrategyConfiguration->setBiddingScheme($biddingScheme);

        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        // Set network targeting (optional).
        $networkSetting = new NetworkSetting();
        $networkSetting->setTargetGoogleSearch(true);
        $networkSetting->setTargetSearchNetwork(true);
        $networkSetting->setTargetContentNetwork(true);
        $campaign->setNetworkSetting($networkSetting);

        // Set additional settings (optional).
        // Recommendation: Set the campaign to PAUSED when creating it to stop
        // the ads from immediately serving. Set to ENABLED once you've added
        // targeting and the ads are ready to serve.
        $campaign->setStatus(CampaignStatus::PAUSED);
        $campaign->setStartDate(date('Ymd', strtotime('+1 day')));
        $campaign->setEndDate(date('Ymd', strtotime('+1 month')));

        // Set frequency cap (optional).
        $frequencyCap = new FrequencyCap();
        $frequencyCap->setImpressions(5);
        $frequencyCap->setTimeUnit(TimeUnit::DAY);
        $frequencyCap->setLevel(Level::ADGROUP);
        $campaign->setFrequencyCap($frequencyCap);

        // Set advanced location targeting settings (optional).
        $geoTargetTypeSetting = new GeoTargetTypeSetting();
        $geoTargetTypeSetting->setPositiveGeoTargetType(
            GeoTargetTypeSettingPositiveGeoTargetType::DONT_CARE
        );
        $geoTargetTypeSetting->setNegativeGeoTargetType(
            GeoTargetTypeSettingNegativeGeoTargetType::DONT_CARE
        );
        $campaign->setSettings([$geoTargetTypeSetting]);

        // Create a campaign operation and add it to the operations list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;

        // Create a campaign with only required settings.
        $campaign = new Campaign();
        $campaign->setName('Interplanetary Cruise #' . uniqid());
        $campaign->setAdvertisingChannelType(AdvertisingChannelType::DISPLAY);

        // Set shared budget (required).
        $campaign->setBudget(new Budget());
        $campaign->getBudget()->setBudgetId($budget->getBudgetId());

        // Set bidding strategy (required).
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType(
            BiddingStrategyType::MANUAL_CPC
        );
        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        $campaign->setStatus(CampaignStatus::PAUSED);

        // Create a campaign operation and add it to the operations list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;

        // Create the campaigns on the server and print out some information for
        // each created campaign.
        $result = $campaignService->mutate($operations);
        foreach ($result->getValue() as $campaign) {
            printf(
                "Campaign with name '%s' and ID %d was added.\n",
                $campaign->getName(),
                $campaign->getId()
            );
        }
    }
}
