NAME = laravelsampleapp
VERSION = 1

ssh:
	docker exec -it $(NAME)_web_1 /bin/bash

composer:
	docker run --rm -v $$(pwd):/app composer/composer install --ignore-platform-reqs

composer-update:
	docker run --rm -v $$(pwd):/app composer/composer update --ignore-platform-reqs

bower:
	rm -rf node_modules/* && docker run -e LOCAL_USER_ID=$$(id -u $(USER)) --rm -v $$(pwd):/data ngtrieuvi92/nodejs-devtools bower install --allow-root

dump:
	php composer.phar dump-autoload

dev_up:
	docker-compose up -d

down:
	docker-compose down

remove:
	docker-compose rm -f

ps:
	docker-compose ps

generate-key:
	docker-compose exec app php artisan key:generate

generate-jwt:
	docker-compose exec app php artisan jwt:secret

cache-clear:
	docker-compose exec app php artisan cache:clear

config-clear:
	docker-compose exec app php artisan config:clear

view-clear:
	docker-compose exec app php artisan view:clear

migrate:
	docker-compose exec app php artisan migrate

optimize:
	docker-compose exec app php artisan optimize

logs:
	docker-compose logs

stop:
	docker stop $$(docker ps -a -q)

delete:
	docker rm $$(docker ps -a -q)

watch-sass:
	node-sass --watch public/sass/root.scss public/custom.css

build-sass:
	node-sass public/sass/root.scss public/custom.css


