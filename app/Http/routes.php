<?php
/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

Route::get(
    '/',
    function () {
        return view('main');
    }
);
Route::match(
    ['get', 'post'],
    'get-campaigns',
    'AdWordsApiController@getCampaignsAction'
);
Route::match(
    ['get', 'post'],
    'download-report',
    'AdWordsApiController@downloadReportAction'
);

Route::get('/customers', 'AdWordsApiController@customers'); // get customers => completed
Route::get('/getCustomersInfo', 'AdsController@getCustomersInfo'); // has permission to get => completed
Route::get('/getAccountHierarchy', 'AdWordsApiController@getAccountHierarchy'); // get all account of a customer => not completed

// Route::get('/create-campaign', 'AdsController@campaign');
//Route::get('/create-campaign', 'AdsController@campaign');

// Format data
Route::get('/getCustomers', 'GoogleAdsWorkController@getCustomersAndFormatData');
Route::get('/getAccountsOfCustomer', 'GoogleAdsWorkController@getAccountHierarchy');    // get structure for node
Route::get('/getAllCampaignOfAllAccounts', 'GoogleAdsWorkController@getAllCampaignOfAllAccounts');

// customerID/campaignID > customerID chính là account con được tạo ra từ master account
// 9188205350/8162228589
Route::get('/getAdGroups/{customerID}/{campaignID}', 'GoogleAdsWorkController@getAdGroups');
Route::get('/getAdGroups1/{customerID}/{campaignID}', 'GoogleAdsWorkController@getAdGroups1');
// /createCampaign/9188205350
Route::get('/createCampaign/{customerID}', 'GoogleAdsWorkController@createCampaign');


Route::get('/create-campaign', 'CampaignController@main');