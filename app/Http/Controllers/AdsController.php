<?php
/**
 * Created by PhpStorm.
 * User: hcenter
 * Date: 11/18/19
 * Time: 10:15
 */

namespace App\Http\Controllers;

use GetOpt\GetOpt;
use App\Helpers\ArgumentNames;
use App\Helpers\ArgumentParser;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsClient;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsClientBuilder;
use Google\Ads\GoogleAds\Lib\V2\GoogleAdsException;
use Google\Ads\GoogleAds\Lib\OAuth2TokenBuilder;
use Google\Ads\GoogleAds\Util\V2\ResourceNames;
use Google\Ads\GoogleAds\V2\Errors\GoogleAdsError;
use Google\ApiCore\ApiException;
use Google\Ads\GoogleAds\V2\Resources\Customer;
use Google\Protobuf\BoolValue;
use Google\Protobuf\StringValue;
use Google\Ads\GoogleAds\V2\Enums\CampaignExperimentTrafficSplitTypeEnum\CampaignExperimentTrafficSplitType;
use Google\Ads\GoogleAds\V2\Resources\CampaignExperiment;
use Google\Ads\GoogleAds\V2\Services\GoogleAdsRow;


class AdsController extends Controller
{
    const CUSTOMER_ID = 7455002068;
    const MANAGER_CUSTOMER_ID = 8549117974;
    const PAGE_SIZE = 1000;

    const BASE_CAMPAIGN_ID = 8168175782;
    const DRAFT_ID = 123456789;

    public function getCustomersInfo()
    {
        // Either pass the required parameters for this example on the command line, or insert them
        // into the constants above.
        $options = (new ArgumentParser())->parseCommandArguments([
            ArgumentNames::CUSTOMER_ID => GetOpt::REQUIRED_ARGUMENT
        ]);
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile(base_path('google_ads_php.ini'))->build();
        // Construct a Google Ads client configured from a properties file and the
        // OAuth2 credentials above.
        $googleAdsClient = (new GoogleAdsClientBuilder())->fromFile(base_path('google_ads_php.ini'))
            ->withOAuth2Credential($oAuth2Credential)
            ->build();

        try {
            self::runGetCustomerInfo(
                $googleAdsClient,
                $options[ArgumentNames::CUSTOMER_ID] ?: self::CUSTOMER_ID
            );
        } catch (GoogleAdsException $googleAdsException) {
            printf(
                "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
                $googleAdsException->getRequestId(),
                PHP_EOL,
                PHP_EOL
            );
            foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
                /** @var GoogleAdsError $error */
                printf(
                    "\t%s: %s%s",
                    $error->getErrorCode()->getErrorCode(),
                    $error->getMessage(),
                    PHP_EOL
                );
            }
        } catch (ApiException $apiException) {
            printf(
                "ApiException was thrown with message '%s'.%s",
                $apiException->getMessage(),
                PHP_EOL
            );
        }
    }
    /**
     * Runs the example.
     *
     * @param GoogleAdsClient $googleAdsClient the Google Ads API client
     * @param int $customerId the customer ID
     */
    public static function runGetCustomerInfo(GoogleAdsClient $googleAdsClient, int $customerId)
    {
        // Issues a getCustomer() request and gets the result.
        $customerServiceClient = $googleAdsClient->getCustomerServiceClient();
        $customer = $customerServiceClient->getCustomer(ResourceNames::forCustomer($customerId));
        // Print information about the account.
        printf(
            "Customer with ID %d, descriptive name '%s', currency code '%s', timezone '%s', "
            . "tracking URL template '%s' and auto tagging enabled '%s' was retrieved.%s",
            $customer->getId()->getValue(),
            $customer->getDescriptiveName()->getValue(),
            $customer->getCurrencyCode()->getValue(),
            $customer->getTimeZone()->getValue(),
            is_null($customer->getTrackingUrlTemplate())
                ? 'N/A' : $customer->getTrackingUrlTemplate()->getValue(),
            $customer->getAutoTaggingEnabled()->getValue() ? 'true' : 'false',
            PHP_EOL
        );
    }

    public function createCustomer()
    {
        // Either pass the required parameters for this example on the command line, or insert them
        // into the constants above.
        $options = (new ArgumentParser())->parseCommandArguments([
            ArgumentNames::MANAGER_CUSTOMER_ID => GetOpt::REQUIRED_ARGUMENT
        ]);
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile(base_path('google_ads_php.ini'))->build();
        // Construct a Google Ads client configured from a properties file and the
        // OAuth2 credentials above.
        $googleAdsClient = (new GoogleAdsClientBuilder())
            ->fromFile(base_path('google_ads_php.ini'))
            ->withOAuth2Credential($oAuth2Credential)
            ->build();
        try {
            self::runExampleCustomer(
                $googleAdsClient,
                $options[ArgumentNames::MANAGER_CUSTOMER_ID] ?: self::MANAGER_CUSTOMER_ID
            );
        } catch (GoogleAdsException $googleAdsException) {
            printf(
                "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
                $googleAdsException->getRequestId(),
                PHP_EOL,
                PHP_EOL
            );
            foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
                /** @var GoogleAdsError $error */
                printf(
                    "\t%s: %s%s",
                    $error->getErrorCode()->getErrorCode(),
                    $error->getMessage(),
                    PHP_EOL
                );
            }
        } catch (ApiException $apiException) {
            printf(
                "ApiException was thrown with message '%s'.%s",
                $apiException->getMessage(),
                PHP_EOL
            );
        }
    }

    /**
     * Runs the example.
     *
     * @param GoogleAdsClient $googleAdsClient the Google Ads API client
     * @param int $managerCustomerId the manager customer ID
     */
    public static function runExampleCustomer(GoogleAdsClient $googleAdsClient, int $managerCustomerId)
    {
        $customer = new Customer([
            'descriptive_name' => new StringValue(
                ['value' => 'Account created with CustomerService on ' . date('Ymd h:i:s')]
            ),
            // For a list of valid currency codes and time zones see this documentation:
            // https://developers.google.com/adwords/api/docs/appendix/codes-formats.
            'currency_code' => new StringValue(['value' => 'USD']),
            'time_zone' => new StringValue(['value' => 'America/New_York']),
            // The below values are optional. For more information about URL
            // options see: https://support.google.com/google-ads/answer/6305348.
            'tracking_url_template' => new StringValue(['value' => '{lpurl}?device={device}']),
            'final_url_suffix' => new StringValue([
                'value' => 'keyword={keyword}&matchtype={matchtype}&adgroupid={adgroupid}'
            ]),
            'has_partners_badge' => new BoolValue(['value' => false])
        ]);
        // Issues a mutate request to create an account
        $customerServiceClient = $googleAdsClient->getCustomerServiceClient();
        $response = $customerServiceClient->createCustomerClient($managerCustomerId, $customer);
        printf(
            'Created a customer with resource name "%s" under the manager account with '
            . 'customer ID %d.%s',
            $response->getResourceName(),
            $managerCustomerId,
            PHP_EOL
        );
    }

    public function campaign()
    {
    // Either pass the required parameters for this example on the command line, or insert them
        // into the constants above.
        $options = (new ArgumentParser())->parseCommandArguments([
            ArgumentNames::CUSTOMER_ID => GetOpt::REQUIRED_ARGUMENT,
            ArgumentNames::BASE_CAMPAIGN_ID => GetOpt::REQUIRED_ARGUMENT,
            ArgumentNames::DRAFT_ID => GetOpt::REQUIRED_ARGUMENT
        ]);
        // Generate a refreshable OAuth2 credential for authentication.
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile(base_path('google_ads_php.ini'))->build();
        // Construct a Google Ads client configured from a properties file and the
        // OAuth2 credentials above.
        $googleAdsClient = (new GoogleAdsClientBuilder())
            ->fromFile(base_path('google_ads_php.ini'))
            ->withOAuth2Credential($oAuth2Credential)
            ->build();
        try {
            self::runCampaign(
                $googleAdsClient,
                $options[ArgumentNames::CUSTOMER_ID] ?: self::CUSTOMER_ID,
                $options[ArgumentNames::BASE_CAMPAIGN_ID] ?: self::BASE_CAMPAIGN_ID,
                $options[ArgumentNames::DRAFT_ID] ?: self::DRAFT_ID
            );
        } catch (GoogleAdsException $googleAdsException) {
            printf(
                "Request with ID '%s' has failed.%sGoogle Ads failure details:%s",
                $googleAdsException->getRequestId(),
                PHP_EOL,
                PHP_EOL
            );
            foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
                /** @var GoogleAdsError $error */
                printf(
                    "\t%s: %s%s",
                    $error->getErrorCode()->getErrorCode(),
                    $error->getMessage(),
                    PHP_EOL
                );
            }
        } catch (ApiException $apiException) {
            printf(
                "ApiException was thrown with message '%s'.%s",
                $apiException->getMessage(),
                PHP_EOL
            );
        }
    }

    /**
     * Runs the example.
     *
     * @param GoogleAdsClient $googleAdsClient the Google Ads API client
     * @param int $customerId the client customer ID
     * @param int $baseCampaignId the base campaign ID
     * @param int $draftId the draft ID used to create an experiment
     */
    public static function runCampaign(
        GoogleAdsClient $googleAdsClient,
        int $customerId,
        int $baseCampaignId,
        int $draftId
    ) {
        // Creates a campaign experiment.
        $campaignExperiment = new CampaignExperiment([
            'campaign_draft' => new StringValue([
                'value' => ResourceNames::forCampaignDraft($customerId, $baseCampaignId, $draftId)
            ]),
            'name' => new StringValue(['value' => 'Campaign Experiment #' . uniqid()]),
            'traffic_split_percent' => 50,
            'traffic_split_type' => CampaignExperimentTrafficSplitType::RANDOM_QUERY
        ]);
        // Issues an asynchronous request to create the campaign experiment. This will return
        // `Google\LongRunning\Operation` (LRO).
        $campaignExperimentServiceClient = $googleAdsClient->getCampaignExperimentServiceClient();
        $operationResponse = $campaignExperimentServiceClient->createCampaignExperiment(
            $customerId,
            $campaignExperiment
        );
        // Prints some info about the process and the campaign experiment resource name.
        $campaignExperimentResourceName =
            $operationResponse->getMetadata()->getCampaignExperiment();
        printf(
            'Asynchronous request to create campaign experiment with resource name "%1$s"'
            . ' started.%2$sWaiting until operation completes.%2$s',
            $campaignExperimentResourceName,
            PHP_EOL
        );
        // pollUntilComplete() implements a default back-off policy for retrying. You can tweak the
        // retrying parameters like the maximum polling interval to use by passing them as an array
        // to the pollUntilComplete() function. Visit the OperationResponse.php file for more
        // details.
        $operationResponse->pollUntilComplete();
        // Fetches information about the created experiment campaign and prints out its resource
        // name.
        $query = "SELECT campaign_experiment.experiment_campaign FROM campaign_experiment"
            . " WHERE campaign_experiment.resource_name = '$campaignExperimentResourceName'";
        $googleAdsServiceClient = $googleAdsClient->getGoogleAdsServiceClient();
        $response = $googleAdsServiceClient->search($customerId, $query);
        /** @var GoogleAdsRow $googleAdsRow */
        $googleAdsRow = $response->getIterator()->current();
        printf(
            "Experiment campaign with resource name '%s' finished creating.%s",
            $googleAdsRow->getCampaignExperiment()->getExperimentCampaignUnwrapped(),
            PHP_EOL
        );
    }
}